/* eslint-disable import/prefer-default-export */
import MyComponent from './components/MyComponent.react';
import ThemeProvider from './components/ThemeProvider.react';

export {
    MyComponent,
    ThemeProvider
};
