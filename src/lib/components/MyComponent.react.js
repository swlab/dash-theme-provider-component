import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

let Input = styled.input`
    /* to avoid using functions this lib may help https://github.com/erikras/styled-components-theme */
    color: ${props => props.theme.maincolor};
        
`


/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
const MyComponent = ({ id, label, setProps, value }) => (
    // const {id, label, setProps, value} = this.props;

    <div id={id}>
        ExampleComponent: {label}&nbsp;
        <Input
            value={value}
            onChange={
                /*
                 * Send the new value to the parent component.
                 * setProps is a prop that is automatically supplied
                 * by dash's front-end ("dash-renderer").
                 * In a Dash app, this will update the component's
                 * props and send the data back to the Python Dash
                 * app server if a callback uses the modified prop as
                 * Input or State.
                 */
                e => setProps({ value: e.target.value })
            }
        />
    </div>
);


MyComponent.defaultProps = {};

MyComponent.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * A label that will be printed when this component is rendered.
     */
    label: PropTypes.string.isRequired,

    /**
     * The value displayed in the input.
     */
    value: PropTypes.string,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};

export default MyComponent