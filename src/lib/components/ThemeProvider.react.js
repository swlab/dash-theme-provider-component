import { ThemeProvider } from 'styled-components';

/**
* Dash-assigned callback that should be called to report property changes
* to Dash, to make them available for callbacks.
*/
const MyThemeProvider = props => <ThemeProvider id={props.id} theme={props.theme}>{props.children}</ThemeProvider>

MyThemeProvider.defaultProps = {};

MyThemeProvider.propTypes = {
    /**
    * Dash-assigned callback that should be called to report property changes
    * to Dash, to make them available for callbacks.
    */
    id: PropTypes.string,
    theme: PropTypes.object,
    children: PropTypes.array  // !!! has to be declared expicitly
};

export default MyThemeProvider