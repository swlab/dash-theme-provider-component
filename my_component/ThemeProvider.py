# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class ThemeProvider(Component):
    """A ThemeProvider component.
Dash-assigned callback that should be called to report property changes
to Dash, to make them available for callbacks.

Keyword arguments:
- children (optional)
- id (optional): Dash-assigned callback that should be called to report property changes
to Dash, to make them available for callbacks.
- theme (optional)"""
    @_explicitize_args
    def __init__(self, children=None, id=Component.UNDEFINED, theme=Component.UNDEFINED, **kwargs):
        self._prop_names = ['children', 'id', 'theme']
        self._type = 'ThemeProvider'
        self._namespace = 'my_component'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['children', 'id', 'theme']
        self.available_wildcard_properties =            []

        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}

        for k in []:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(ThemeProvider, self).__init__(children=children, **args)
