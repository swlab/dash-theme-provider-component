import my_component
import dash
from dash.dependencies import Input, Output
import dash_html_components as html

theme = {
    'maincolor': 'blue'
}

app = dash.Dash(__name__)
app.layout = my_component.ThemeProvider(
        id='themeprovider',
        children=[
            my_component.MyComponent(
                id='input',
                value='my-value',
                label='my-label'
            ),
            html.Div(id='output')
        ],
        theme=theme
    )


@app.callback(Output('output', 'children'), [Input('input', 'value')])
def display_output(value):
    return 'You have entered {}'.format(value)


if __name__ == '__main__':
    app.run_server(debug=True)
